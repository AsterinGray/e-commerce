<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class uniquepassword implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */

    // public function passes($messages, $rules)
    // {
    // if(
    //     // $messages == [
    //     //     'email.required' => 'We need to know your e-mail address!',
    //     //     'password.required' => 'How will you log in?',
    //     //     'password.confirmed' => 'Passwords must match...',
    //     //     'password.regex' => 'Regex!'
    //     // ] &&
    //     $rules == [
    //         'name' => ['required', 'string', 'max:255'],
    //         'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
    //         'phone' => ['required', 'string', 'max:15'],
    //         'username' => ['required', 'string', 'min:5', 'unique:users'],
    //         'dob' => ['required', 'string'],
    //         'password' => [
    //             'required',
    //             'string',
    //             'min:8',
    //             'confirmed',
    //             'regex/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%]).*$/'
    //         ]
        
    //         ]
    // )
    //     return true;
    //     // return uniquepassword::make($messages, $rules);
    // }

    public function passes($attribute, $value)
{
    return preg_match("/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,255}$/", $value);
}
    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Password must contain at least one number, both uppercase and lowercase letters, and minimum 8 characters.';
    }
}
