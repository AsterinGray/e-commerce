<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    //
    public function index() { 
        $product = DB::table('product')->get();

        return view('admin', ['product' => $product]);
    }

    public function shop() {
        $product = DB::table('product')->paginate(9);
        return view('shop', ['product' => $product]);
    }
    
    public function product($id) {
        $product = DB::table('product')->where('id', $id)->get();

        return view('product', ['product' => $product]);
    }

    public function store(Request $request) {

        DB::table('product')->insert([
            'product_image' => $request->image,
            'product_name' => $request->name,
            'product_ID' => $request->productID,
            'product_description' => $request->description,
            'product_price' => $request->price,
        ]);

        return redirect('/admin');
    }

    public function edit($id) {
        $product = DB::table('product')->where('id', $id)->get();

        return view('edit', ['product' => $product]);
    }

    public function update(Request $request) {
        DB::table('product')->where('id', $request->id)->update([
            'product_image' => $request->image,
            'product_name' => $request->name,
            'product_description' => $request->description,
            'product_price' => $request->price
        ]);

        return redirect('/admin');
    }

    public function clear($id) {
        DB::table('product')->where('id', $id)->delete();

        return redirect('/admin');
    }
}
