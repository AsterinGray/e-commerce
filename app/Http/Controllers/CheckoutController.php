<?php

namespace App\Http\Controllers;
use App\Product;
use Illuminate\Http\Request;
use Cart;
use DB;

class CheckoutController extends Controller
{
    //
    public function index() {
        $item = DB::table('checkout')->get();
        return view('checkout', ['item' => $item]);
    }

    public function add($id) {
        $item = DB::table('product')->where('id', $id)->get();

        $cart = Cart::add(array(
            'item_image' => $item->image,
            'item_name' => $item->name,
            'item_price' => $item->price,
            'item_description' => $item->description,
            'item_qty' => $item->qty,
        ));
        // DB::table('checkout')->insert([
        //     'item_image' => $request->image,
        //     'item_name' => $request->name,
        //     'item_price' => $request->price,
        //     'item_description' => $request->description,
        //     'item_qty' => $request->qty,
        // ]);
        return redirect('/checkout');
    }



}
