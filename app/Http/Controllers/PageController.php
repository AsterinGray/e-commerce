<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function home(){
        return view('homepage');
    }

    public function shop(){
        return view('shop');
    }

    public function about(){
        return view('about');
    }

    public function product(){
        return view('product');
    }

    public function login(){
        return view('auth.login');
    }

    public function regis(){
        return view('auth.register');
    }

    public function admin(){
        return view('admin');
    }

    public function checkout(){
        return view('checkout');
    }
}
