<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $table = 'product';
    // public $timestamps = false;

    // protected $fillable = [
    //     'id', 'product_name', 'product_ID', 'product_description', 'product_price', 'product_image',
    // ];
}
