<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */


    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->bigIncrements('id'); // real ID -> passing ID
            $table->string('product_name');
            $table->string('product_ID');
            $table->string('product_description');
            $table->integer('product_price');
            $table->mediumText('product_image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
