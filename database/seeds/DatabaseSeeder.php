<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Product;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        User::create([
            'name' => 'andi',
            'role' => 'admin',
            'email' => 'andi@gmail.com',
            'phone_number' => '081260119011',
            'username' => 'admin',
            'address' => 'Jln. ABC No. 123',
            'dob' => '2001-02-12',
            'password' => Hash::make('Admin123'),
        ]);

        Product::create([
            'product_name' => 'Bottle',
            'product_ID' => 'ID-001',
            'product_description' => 'Lorem',
            'product_price' => '10000',
            'product_image' => 'bottle.jpg',
        ]);

        Product::create([
            'product_name' => 'Bottle',
            'product_ID' => 'ID-002',
            'product_description' => 'Lorem',
            'product_price' => '10000',
            'product_image' => 'bottle.jpg',
        ]);

        Product::create([
            'product_name' => 'Bottle',
            'product_ID' => 'ID-003',
            'product_description' => 'Lorem',
            'product_price' => '10000',
            'product_image' => 'bottle.jpg',
        ]);

        Product::create([
            'product_name' => 'Bottle',
            'product_ID' => 'ID-004',
            'product_description' => 'Lorem',
            'product_price' => '10000',
            'product_image' => 'bottle.jpg',
        ]);

        Product::create([
            'product_name' => 'Bottle',
            'product_ID' => 'ID-005',
            'product_description' => 'Lorem',
            'product_price' => '10000',
            'product_image' => 'bottle.jpg',
        ]);

        Product::create([
            'product_name' => 'Bottle',
            'product_ID' => 'ID-006',
            'product_description' => 'Lorem',
            'product_price' => '10000',
            'product_image' => 'bottle.jpg',
        ]);

        Product::create([
            'product_name' => 'Bottle',
            'product_ID' => 'ID-007',
            'product_description' => 'Lorem',
            'product_price' => '10000',
            'product_image' => 'bottle.jpg',
        ]);

        Product::create([
            'product_name' => 'Bottle',
            'product_ID' => 'ID-008',
            'product_description' => 'Lorem',
            'product_price' => '10000',
            'product_image' => 'bottle.jpg',
        ]);

        Product::create([
            'product_name' => 'Bottle',
            'product_ID' => 'ID-009',
            'product_description' => 'Lorem',
            'product_price' => '10000',
            'product_image' => 'bottle.jpg',
        ]);
        
        Product::create([
            'product_name' => 'Bottle',
            'product_ID' => 'ID-010',
            'product_description' => 'Lorem',
            'product_price' => '10000',
            'product_image' => 'bottle.jpg',
        ]);
    }
}
