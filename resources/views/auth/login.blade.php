@extends('layout.main')

@section('title','Login')

@section('css')
    <link rel="stylesheet" href="{{asset('css/Login-Register.css')}}">
    <link rel="stylesheet" href="{{asset('asset/lib/fontawesome.css')}}">
@endsection

@section('content')
    <section class="Login">
        <div class="Box">
            <h1>LOGIN</h1>
            <form method="POST" action="/login">
                @csrf
                <span class="input-box">
                    <i class="fa fa-user icon"></i>
                    <input type="email" placeholder="email" class="@error('email') is-invalid @enderror" id="email" name="email" autocomplete="email" autofocus required>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </span>

                <span class="input-box">
                    <i class="fa fa-key icon"></i>
                    <input type="password" placeholder="Password" minlength="8" class="@error('password') is-invalid @enderror" id="password" name="password" value="{{ old('password') }}" autocomplete="password" required>
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </span>
                
                <p>Don't have any account yet? <a href="/register">click here</a></p>
                <button type="submit">LOGIN</button>
            </form>
        </div>
    </section>
@endsection