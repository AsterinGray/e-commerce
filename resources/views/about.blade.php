@extends('layout.main')

@section('title', 'SGuna')

@section('css')
    <link rel="stylesheet" href="{{asset('css/about.css')}}">
@endsection

@section('content')
<h1 class="Judul">SGuna</h1>
<div class="content-area">
    <div class="Content">
        <img src="{{asset('asset/img/About/browsing-illustration.svg')}}" class="Content-Picture">
        <div class="Content-Def">
            <h1 class="Content-Def-Judul">Best Shopping Experience</h1>
            <p class="Content-Def-Keterangan">We always try to give user the best experience
                they could get while shopping here. Even though we are still new in this expertise,
                we will keep learning to give you the best experience you can get.
            </p>
        </div>
    </div>
    <div class="Content content-2">
        <div class="Content-Def" id="Content-Def-2">
            <h1 class="Content-Def-Judul">Implement Innovative Idea</h1>
            <p class="Content-Def-Keterangan">Day to day new technology keep developing no different from us.
                We keep learning the new technology so we can come up with new idea and implement it here.
            </p>
        </div>
        <img src="{{asset('asset/img/About/lightbulb-illustration.svg')}}" class="Content-Picture">
    </div>
    <div class="Content">
        <img src="{{asset('asset/img/About/remotely-illustration.svg')}}" class="Content-Picture">
        <div class="Content-Def">
            <h1 class="Content-Def-Judul">Covid-19 Don't Slow Us Down</h1>
            <p class="Content-Def-Keterangan">We know that pandemic of COVID-19 is a really big problem to
                everyone. But, we always try to develop this further. Even though you are at home but you can still enjoy.
            </p>
        </div>
    </div>
</div>
@endsection