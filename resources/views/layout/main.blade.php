<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    @yield('css')
</head>
<body>
    <nav class="navbar">
        <label for="hamburger">&#9776;</label>
        <input type="checkbox" id="hamburger">
        <span class="navbar-logo">
            <a href="{{ url('/') }}">SGuna</a>
        </span>
        <span class="navbar-link">
            <a href="{{ url('/') }}" id="home">Home</a>
            <a href="{{ url('/shop') }}">Shop</a>
            <a href="{{ url('/about') }}">About</a>
        </span>
        <span class="navbar-button">
            <span>
                <img src="{{ asset('asset/img/Hompage/cart-icon.svg') }}" alt="Cart-icon">
                <a href="{{ url('/checkout') }}">Cart</a>
            </span>
            <span>
                <img src="{{ asset('asset/img/Hompage/user-icon.svg') }}" alt="User-icon">
                @guest
                    <a href="{{ url('/login') }}">Login</a><span>|</span><a href="{{ url('/register') }}">Register</a>
                @else
                    {{ Auth::user()->name }} | 
                    <a href="{{ route('logout') }}" onclick="document.getElementById('logout-form').submit();">Logout</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                @endguest
            </span>
        </span>
    </nav>
    @yield('content')
    <footer>
        <div class="footer-content">
            <span>SGuna</span>
            <span>
                <p>Do you get any question ?</p>
                <p>Call us on Monday-Friday 10am-5pm</p>
                <p>Regarding our product <a href="tel:+628827068245">+62 882-7068-2425</a></p>
            </span>
        </div>
        <hr>
        <span>&copy; E-commerce Serba Guna</span>
    </footer>
    <script src="{{ asset('js/app.js') }}" defer></script>
    @yield('js')
</body>
</html>