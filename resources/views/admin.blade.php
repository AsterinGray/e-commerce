@extends('layout.main')

@section('title', 'SGuna')

@section('css')
    <link rel="stylesheet" href="{{asset('css/admin.css')}}">
@endsection

@section('content')
<section class="admin">
    <table class="table-content">
        <tr>
            <th>Name</th>
            <th>Image</th>
            <th>Description</th>
            <th>Price</th>
            <th>ID</th> 
            <th>Action</th>
        </tr>

        @foreach($product as $p)
        <tr>
            <td class="name"> {{ $p->product_name }}</td>
            <td><img src="{{ asset('uploads/product/' . $p->product_image) }}" alt="product-bot" width=100px height=100px></td>
            <td>
                {{ $p->product_description }}
            </td>
            <td>{{ $p->product_price }}</td>
            <td>{{ $p->product_ID }}</td>
            <td>
                <button class="del">
                    <a href="/admin/delete/{{ $p->id }}">Delete</a>
                </button>
                <button class="edit">
                    <a href="/admin/edit/{{ $p->id }}">Edit</a>
                </button>
            </td>
        </tr>
        @endforeach
    </table>
    <section class="add">
    
        <div class="add-head">
            Add New Item
        </div>
        <form action="/admin/store" method="post" class="add-content">

            {{ csrf_field() }}
            
            <input type="text" name="name" placeholder="Product Name" required="required"></input>
            <label for="image">
                <img src="" alt="Image Preview" id="image-preview">
            </label>
            <input name="image" type="file" id="image">

            <textarea name="description" placeholder="Product Description" required="required"></textarea>

            <input name="productID" type="text" placeholder="Product ID" required="required"></input>

            <input name="price" type="number" placeholder="Product Price" required="required"></input>

            <button type="submit" class="item">Add Item</button>

        </form>
    </section>
</section>
@endsection

@section('js')
    <script src="{{asset('asset/lib/jquery.js')}}"></script>
    <script src="{{asset('asset/js/admin.js')}}"></script>
@endsection