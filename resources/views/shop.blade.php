@extends('layout.main')

@section('title','Shop')

@section('css')
    <link rel="stylesheet" href="{{asset('css/shop.css')}}">
@endsection

@section('content')
    <section class="description">
        <h1>Brings Your Need Home</h1>
        <p>We always support all your needs to make your life easier and happier.</p>
        <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. 
            Beatae eum aliquid laborum minus explicabo molestiae dolorem doloribus delectus! 
            Recusandae eaque harum ab quis est. Eum ad sequi vitae accusamus explicabo?
        </p>
    </section>
    <section class="product">

        <h1>Our Product</h1>
        
        <div class="product-layout">
            @foreach($product as $p)
            <div class="product-layout-card">
                <!-- <img src="{{asset('asset/img/Shop/2662397.jpg')}}" alt="product-image"> -->
                <img src="{{ asset('uploads/product/' . $p->product_image) }}" alt="product-image">

                <h2>{{ $p->product_name }}</h2>
                <p>
                    {{ $p->product_description }}
                </p>

                <a href="/shop/product/{{ $p->id }}" class="product-layout-card-button">
                    <div>{{ $p->product_price }}</div>
                    <div>See More Detail</div>
                </a>
            </div>
            @endforeach
        </div>
            Page : {{ $product->currentPage() }} <br>
            {{ $product->links() }}
    </section>
    <section class="conclusion">
        <h1>Shop The Product You Need</h1>
        <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. 
            Eaque architecto nihil quia doloremque nesciunt, 
            dolores impedit dicta accusamus fugiat quam enim natus 
            necessitatibus ea commodi recusandae itaque tempore optio voluptatum.
        </p>
    </section>
@endsection