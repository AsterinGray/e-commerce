@extends('layout.main')

@section('title', 'SGuna')

@section('css')
    <link rel="stylesheet" href="{{asset('css/admin.css')}}">
@endsection

@section('content')

<section class="edit">

    <div class="edit-head">
        Edit Item
    </div>

    @foreach($product as $p)

    <form action="/admin/update" method="post" class="edit-content">

        {{ csrf_field() }}
		<input type="hidden" name="id" value="{{ $p->id }}"> 

        <input type="text" name="name" placeholder="Product Name" required="required" value="{{ $p->product_name }}"></input>

        <input type="text" placeholder="Product Image"></input>
        <label for="image">
            <img src="{{asset('asset/img/Product/bot.jpg')}}" alt="Image Preview"  id="image-preview" width=100px height=100px>
        </label>
        <input name="image" type="file" id="image" value="{{ $p->product_image }}">

        <textarea name="description" placeholder="Product Description" required="required">{{ $p->product_description }}</textarea>

        <input name="price" type="number" placeholder="Product Price" required="required" value="{{ $p->product_price }}"></input>

        <input type="submit" class="item"></input>

    </form>
    @endforeach
</section>

@endsection

@section('js')
@endsection