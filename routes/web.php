<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/','PageController@home');
Route::get('/about', 'PageController@about');
Route::get('/login', 'PageController@login');
Route::get('/register', 'PageController@regis');

// Logout
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

//Shop
Route::get('/shop', 'ProductController@shop');
Route::get('/shop/product/{id}', 'ProductController@product');

// Checkout
Route::get('/checkout', 'CheckoutController@index');
Route::get('/checkout/add/{id}', 'CheckoutController@add');

//ADMIN PANEL
Route::get('/admin', 'ProductController@index');
Route::post('/admin/store', 'ProductController@store');

// edit Route: admin
Route::get('/admin/edit/{id}', 'ProductController@edit');
Route::post('/admin/update', 'ProductController@update');

// delete Route: admin
Route::get('/admin/delete/{id}', 'ProductController@clear');




